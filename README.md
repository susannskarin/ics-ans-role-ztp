ics-ans-role-ztp
===================

Ansible role to install ztp.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ztp
```

License
-------

BSD 2-clause
